package eu.axes.services.shotextractor;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.Iterator;

import javax.jws.WebService;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Video;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;

import eu.axes.utils.AxesAnnotator;
import eu.axes.utils.NamingSchemeUtils;

/**
 * This class is a wrapper for the ShotExtraction component provided by Technicolor.
 * In current version, it handles a directory where frames, I-frames, fps and scene cuts are provided (after calling two command line utilities).
 *
 * Prior to call the utility it first get the video file pointer, then check whether the shot's had already been computed (to prevent calling scripts for nothing) and finally enriched the received
 * resource with annotations coming from that directory folder.
 *
 * @author ymombrun
 * @date 2014-11-20
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public final class ShotExtractor implements Analyser {


	/**
	 * The configuration bean used inside
	 */
	private final ShotExtractorConfigBean conf;


	/**
	 * The logger used inside
	 */
	private final Log log;


	/**
	 * The content manager in charge of reading and writing the native content
	 */
	private final ContentManager contentManager;


	/**
	 * The constructor from a configuration bean
	 *
	 * @param conf
	 *            The configuration
	 * @throws WebLabCheckedException
	 *             If the configuration is not valid.
	 */
	public ShotExtractor(final ShotExtractorConfigBean conf) throws WebLabCheckedException {
		this.log = LogFactory.getLog(this.getClass());
		this.contentManager = ContentManager.getInstance();
		this.conf = conf;
		this.conf.validateBean();
		this.log.info("Shot Extractor service successfully started.");
	}


	@Override
	public ProcessReturn process(final ProcessArgs args) throws ContentNotAvailableException, InvalidParameterException, UnexpectedException, InsufficientResourcesException {
		this.log.trace("Process method called.");

		final AxesAnnotator aa = this.checkArgs(args);
		final Document document = (Document) args.getResource();
		final String uri = document.getUri();

		this.log.debug("Document '" + uri + "' is valid. Check if it has already been processed.");

		final String collectionId = aa.readCollectionId().firstTypedValue();
		final String videoId = aa.readVideoId().firstTypedValue();

		final File outPath = new File(new File(this.conf.getPathToOutFolder(), collectionId), videoId);

		final ShotDirectoryParser parser = new ShotDirectoryParser(outPath, document, this.conf);

		if (parser.isValid()) {
			this.log.info("Document '" + uri + "' as already been processed in '" + outPath + "'. Skipping access to content and call to scripts.");
		} else {
			final File videoFile = this.retrieveFileOfPreferredFormat(document, collectionId, videoId);

			if ((outPath.exists() && !outPath.isDirectory()) || (!outPath.exists() && !outPath.mkdirs())) { // Create out folders if needed
				throw this.createAndLogIRE("Unable to create directory '" + outPath + "' for document '" + uri + "'.");
			}

			if (parser.isScenecutValid()) {
				this.log.debug("Scenecut file has already been generated for Document '" + uri + "'in '" + outPath + "'. Skipping shot_detector.");
			} else {
				this.callShotDetector(videoFile, outPath);
			}

			if (!parser.isScenecutValid()) {
				throw this.createAndLogUE("After shot_detector script, the folder " + outPath + " is still not valid for Document " + uri + ".");
			}

			this.callFrameExtractor(videoFile, outPath);

			if (!parser.isValid()) {
				throw this.createAndLogUE("After calling the scripts the folder '" + outPath + "' does not contain the files that should have been generated.");
			}
		}

		try {
			parser.enrichWithShotAndFrames();
		} catch (final IOException ioe) {
			throw this.createAndLogUE("An error occurs parsing generated files for Document " + uri + ".", ioe);
		}

		this.log.debug("Results successfully parsed for Document " + uri + ".");

		final ProcessReturn processReturn = new ProcessReturn();
		processReturn.setResource(document);
		return processReturn;
	}


	private void callFrameExtractor(final File videoFile, final File outputDir) throws UnexpectedException {
		final CommandLine cmdLine = CommandLine.parse(this.conf.getPathToFrameExtractor() + " " + videoFile + " " + outputDir);
		this.callCommandLine(cmdLine, this.conf.getPathToFrameExtractor().getParentFile(), outputDir);
	}


	private void callShotDetector(final File videoFile, final File outputDir) throws UnexpectedException {
		final CommandLine cmdLine = CommandLine.parse(this.conf.getPathToShotDetector() + " " + videoFile + " " + outputDir);
		this.callCommandLine(cmdLine, this.conf.getPathToShotDetector().getParentFile(), outputDir);
	}


	private void callCommandLine(final CommandLine cmdLine, final File workingdirectory, final File outputDir) throws UnexpectedException {
		final DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		final ExecuteWatchdog watchdog = new ExecuteWatchdog(this.conf.getTimeout());

		try (final ByteArrayOutputStream baos = new ByteArrayOutputStream();) {
			try {
				final Executor executor = new DefaultExecutor();
				executor.setExitValue(0);
				executor.setWatchdog(watchdog);
				executor.setWorkingDirectory(workingdirectory);
				executor.setStreamHandler(new PumpStreamHandler(baos));

				this.log.debug("Executing commandline " + cmdLine.toString());

				try {
					executor.execute(cmdLine, this.conf.getEnvironmentMap(), resultHandler);
				} catch (final Exception e) {
					throw this.createAndLogUE("An error occurs executing command line: " + cmdLine.toString(), e);
				}

				try {
					resultHandler.waitFor();
				} catch (final Exception e) {
					throw this.createAndLogUE("An error occurs executing command line: " + cmdLine.toString(), e);
				}
			} finally {
				try {
					FileUtils.write(new File(outputDir, FilenameUtils.getBaseName(cmdLine.getExecutable()) + ".log"), baos.toString("utf-8"), "utf-8");
				} catch (final IOException ioe) {
					this.log.warn("An error occurs writing output of " + cmdLine.getExecutable() + " into a log file.", ioe);
				}
			}
		} catch (final IOException ioe) {
			this.log.warn("An error occurs closing output stream of " + cmdLine.getExecutable() + ".", ioe);
		}

		if (resultHandler.getExitValue() != 0) {
			throw this.createAndLogUE("The exitValue is not " + 0 + " but " + resultHandler.getExitValue() + " for the command line: " + cmdLine.toString());
		}

		this.log.debug("Commandline " + cmdLine.toString() + " has been successfully executed.");
	}



	/**
	 * Retrieves the Content file of the preferred type (configured in the {@link ShotExtractorConfigBean}). If not available an error is thrown.
	 *
	 * @param resource
	 *            The resource in which search for normalised content.
	 * @param collectionId
	 *            The collection id used in the content URI naming scheme.
	 * @param videoId
	 *            The collection id used in the content URI naming scheme.
	 * @return A {@link File} pointing to the preferred content available.
	 * @throws ContentNotAvailableException
	 *             If no content of one of the preferred type is available.
	 */
	private File retrieveFileOfPreferredFormat(final Resource resource, final String collectionId, final String videoId) throws ContentNotAvailableException {
		final String uri = resource.getUri();
		final WProcessingAnnotator wpa = new WProcessingAnnotator(resource);
		final Collection<URI> contentUris = wpa.readNormalisedContent().getValues();
		contentUris.addAll(wpa.readNativeContent().getValues());
		final Iterator<String> prefExt = this.conf.getPreferredExtension().iterator();
		while (prefExt.hasNext()) {
			final String ext = prefExt.next();
			final URI videoUri = NamingSchemeUtils.getVideoResourceURI(collectionId, videoId, ext);
			if (!contentUris.contains(videoUri)) {
				this.log.debug("Preferred format " + ext + " not provided for video " + uri + " testing next extension.");
				continue;
			}
			// Try to get content (the code inside checks existence and readability of the file).
			try {
				return this.contentManager.readContent(videoUri, resource);
			} catch (final WebLabCheckedException wlce) {
				this.log.warn("An error occurs when accessing content of type '" + ext + "' for video '" + uri + "'. Try another format.");
			}
		}
		throw this.createAndLogCNAE("No content files of the video '" + uri + "' is matching preferred types " + this.conf.getPreferredExtension() + ".");
	}


	/**
	 * Validates the received {@link ProcessArgs} and returns a {@link AxesAnnotator} opened on the {@link Document} that it should contain or fails.
	 *
	 * @param args
	 *            The {@link ProcessArgs} to check compliance with need of the service.
	 * @return The {@link AxesAnnotator} opened on the resource should inside <code>args</code>
	 * @throws InvalidParameterException
	 *             If <code>args</code> is <code>null</code> or does not contain a valid {@link Document}.
	 */
	private AxesAnnotator checkArgs(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			throw this.createAndLogIPE("ProcessArgs was null.");
		}
		final Resource res = args.getResource();
		if (res == null) {
			throw this.createAndLogIPE("Resource in ProcessArgs was null.");
		}
		final String uri = res.getUri();
		if (!(res instanceof Document)) {
			throw this.createAndLogIPE("Resource '" + uri + "' in ProcessArgs was not a Document.");
		}
		final Document doc = (Document) res;
		if (!doc.isSetMediaUnit() || doc.getMediaUnit().isEmpty()) {
			throw this.createAndLogIPE("No MediaUnit in the Document '" + uri + "' inside ProcessArgs.");
		}
		if (doc.getMediaUnit().size() > 1) {
			LogFactory.getLog(this.getClass()).warn("The ShotDetector will only analyse the first MediaUnit for Resource '" + uri + "'.");
		}
		final MediaUnit mu = doc.getMediaUnit().get(0);
		if (!(mu instanceof Video)) {
			throw this.createAndLogIPE("The first MediaUnit in the Document '" + uri + "' is not a Video.");
		}
		final AxesAnnotator aa = new AxesAnnotator(doc);
		if (!aa.readCollectionId().hasValue()) {
			throw this.createAndLogIPE("Input video has no collection id annotated on Document '" + uri + "'.");
		}
		if (!aa.readVideoId().hasValue()) {
			throw this.createAndLogIPE("Input video has no video id annotated on Document '" + uri + "'.");
		}
		return aa;
	}


	/**
	 * Utility method that creates an {@link ContentNotAvailableException} from a message after logging this message.
	 *
	 * @param message
	 *            The message to be logged and thrown
	 * @return The {@link ContentNotAvailableException} to be created
	 */
	private ContentNotAvailableException createAndLogCNAE(final String message) {
		this.log.error(message);
		return ExceptionFactory.createContentNotAvailableException(message);
	}


	/**
	 * Utility method that creates an {@link InvalidParameterException} from a message after logging this message.
	 *
	 * @param message
	 *            The message to be logged and thrown
	 * @return The {@link InvalidParameterException} to be created
	 */
	private InvalidParameterException createAndLogIPE(final String message) {
		this.log.error(message);
		return ExceptionFactory.createInvalidParameterException(message);
	}


	/**
	 * Utility method that creates an {@link InsufficientResourcesException} from a message after logging this message.
	 *
	 * @param message
	 *            The message to be logged and thrown
	 * @return The {@link InsufficientResourcesException} to be created
	 */
	private InsufficientResourcesException createAndLogIRE(final String message) {
		this.log.error(message);
		return ExceptionFactory.createInsufficientResourcesException(message);
	}


	/**
	 * Utility method that creates an {@link UnexpectedException} from a message after logging this message.
	 *
	 * @param message
	 *            The message to be logged and thrown
	 * @return The {@link UnexpectedException} to be created
	 */
	private UnexpectedException createAndLogUE(final String message) {
		this.log.error(message);
		return ExceptionFactory.createUnexpectedException(message);
	}


	/**
	 * Utility method that creates an {@link UnexpectedException} from a message after logging this message.
	 *
	 * @param message
	 *            The message to be logged and thrown
	 * @param cause
	 *            The cause of the error
	 * @return The {@link UnexpectedException} to be created
	 */
	private UnexpectedException createAndLogUE(final String message, final Exception cause) {
		this.log.error(message, cause);
		return ExceptionFactory.createUnexpectedException(message, cause);
	}


}
