package eu.axes.services.shotextractor;

import java.io.File;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;

/**
 * This class is a simple holder for the configuration used in the ShotExtractor service.
 *
 * @author ymombrun
 * @date 2014-11-20
 */
public final class ShotExtractorConfigBean {


	private static final int DEFAULT_TIMEOUT = 1000 * 60 * 30;


	private static final List<String> DEFAULT_PREFERRED_EXT = Arrays.asList("ts", "mp4", "mpg");


	private static final SuffixFileFilter DEFAULT_FRAME_FILTER = new SuffixFileFilter("jpg", IOCase.INSENSITIVE);


	private static final String DEFAULT_MIME_TYPE = "image/jpeg";


	private Map<String, String> environmentMap;


	private IOFileFilter framesFileFilter;


	private final Log log;


	private File pathToShotDetector, pathToFrameExtractor, pathToOutFolder;


	private List<String> preferredExtension;


	private String serviceUri, framesMimeType;


	private URI serviceValidUri;


	private int timeout;


	/**
	 * The constructor
	 */
	public ShotExtractorConfigBean() {
		this.log = LogFactory.getLog(this.getClass());
	}


	/**
	 * @return The Service uri or null.
	 */
	public URI getServiceValidUri() {
		return this.serviceValidUri;
	}


	/**
	 * @throws WebLabCheckedException
	 *             If anything is wrong in the configuration
	 */
	public void validateBean() throws WebLabCheckedException {
		this.checkExecutable("Shot Detector", this.pathToShotDetector);
		this.checkExecutable("Frame Extractor", this.pathToFrameExtractor);

		ShotExtractorConfigBean.checkDirectory("Out Folder", this.pathToOutFolder);

		if ((this.serviceUri == null) || this.serviceUri.isEmpty()) {
			this.log.debug("No service uri provided.");
		} else {
			this.serviceValidUri = URI.create(this.serviceUri);
		}

		if (this.framesFileFilter == null) {
			this.framesFileFilter = ShotExtractorConfigBean.DEFAULT_FRAME_FILTER;
			this.log.debug("No Frames file Filter set. Using default one, i.e. parsing every jpg files.");
		}

		if ((this.preferredExtension == null) || this.preferredExtension.isEmpty()) {
			this.preferredExtension = ShotExtractorConfigBean.DEFAULT_PREFERRED_EXT;
			this.log.debug("No prefered extension set. Using default ones: " + this.preferredExtension);
		}

		if ((this.framesMimeType == null) || this.framesMimeType.isEmpty()) {
			this.framesMimeType = ShotExtractorConfigBean.DEFAULT_MIME_TYPE;
			this.log.debug("No Frames Mime type set. Using default one, i.e. " + this.framesMimeType + ".");
		}

		if (this.timeout <= 0) {
			this.timeout = ShotExtractorConfigBean.DEFAULT_TIMEOUT;
			this.log.debug("No timeout provided or negative one. Using 30 minutes.");
		}

		if (this.environmentMap == null) {
			this.environmentMap = Collections.emptyMap();
		}
	}


	private static void checkDirectory(final String directoryName, final File directoryPath) throws WebLabCheckedException {
		if (directoryPath == null) {
			throw new WebLabCheckedException(directoryName + " not set.");
		}
		if (!directoryPath.exists() && !directoryPath.mkdirs()) {
			throw new WebLabCheckedException(directoryName + " " + directoryPath + " does not exists or can't be created.");
		}
		if (!directoryPath.isDirectory()) {
			throw new WebLabCheckedException(directoryName + " " + directoryPath + " is not a directory.");
		}
		if (!directoryPath.canWrite()) {
			throw new WebLabCheckedException(directoryName + " " + directoryPath + " is not writtable.");
		}
	}


	private void checkExecutable(final String executableName, final File executablePath) throws WebLabCheckedException {
		if (this.pathToShotDetector == null) {
			throw new WebLabCheckedException("Path to " + executableName + " executable not set.");
		}
		if (!executablePath.exists()) {
			throw new WebLabCheckedException("Path to " + executableName + " executable '" + executablePath + "' does not exists.");
		}
		if (!executablePath.isFile()) {
			throw new WebLabCheckedException("Path to " + executableName + " executable '" + executablePath + "' is not a file.");
		}
		if (!executablePath.canRead()) {
			throw new WebLabCheckedException("Path to " + executableName + " executable '" + executablePath + "' is not readible.");
		}
		if (!executablePath.canExecute()) {
			throw new WebLabCheckedException("Path to " + executableName + " executable '" + executablePath + "' is not executable.");
		}
	}


	/**
	 * @return the environmentMap
	 */
	public Map<String, String> getEnvironmentMap() {
		return this.environmentMap;
	}


	/**
	 * @param environmentMap
	 *            the environmentMap to set
	 */
	public void setEnvironmentMap(final Map<String, String> environmentMap) {
		this.environmentMap = environmentMap;
	}


	/**
	 * @return the framesFileFilter
	 */
	public IOFileFilter getFramesFileFilter() {
		return this.framesFileFilter;
	}


	/**
	 * @param framesFileFilter
	 *            the framesFileFilter to set
	 */
	public void setFramesFileFilter(final IOFileFilter framesFileFilter) {
		this.framesFileFilter = framesFileFilter;
	}


	/**
	 * @return the pathToShotDetector
	 */
	public File getPathToShotDetector() {
		return this.pathToShotDetector;
	}


	/**
	 * @param pathToShotDetector
	 *            the pathToShotDetector to set
	 */
	public void setPathToShotDetector(final File pathToShotDetector) {
		this.pathToShotDetector = pathToShotDetector;
	}


	/**
	 * @return the pathToFrameExtractor
	 */
	public File getPathToFrameExtractor() {
		return this.pathToFrameExtractor;
	}


	/**
	 * @param pathToFrameExtractor
	 *            the pathToFrameExtractor to set
	 */
	public void setPathToFrameExtractor(final File pathToFrameExtractor) {
		this.pathToFrameExtractor = pathToFrameExtractor;
	}


	/**
	 * @return the pathToOutFolder
	 */
	public File getPathToOutFolder() {
		return this.pathToOutFolder;
	}


	/**
	 * @param pathToOutFolder
	 *            the pathToOutFolder to set
	 */
	public void setPathToOutFolder(final File pathToOutFolder) {
		this.pathToOutFolder = pathToOutFolder;
	}


	/**
	 * @return the preferredExtension
	 */
	public List<String> getPreferredExtension() {
		return this.preferredExtension;
	}


	/**
	 * @param preferredExtension
	 *            the preferredExtension to set
	 */
	public void setPreferredExtension(final List<String> preferredExtension) {
		this.preferredExtension = preferredExtension;
	}


	/**
	 * @return the serviceUri
	 */
	public String getServiceUri() {
		return this.serviceUri;
	}


	/**
	 * @param serviceUri
	 *            the serviceUri to set
	 */
	public void setServiceUri(final String serviceUri) {
		this.serviceUri = serviceUri;
	}


	/**
	 * @return the framesMimeType
	 */
	public String getFramesMimeType() {
		return this.framesMimeType;
	}


	/**
	 * @param framesMimeType
	 *            the framesMimeType to set
	 */
	public void setFramesMimeType(final String framesMimeType) {
		this.framesMimeType = framesMimeType;
	}


	/**
	 * @return the timeout
	 */
	public int getTimeout() {
		return this.timeout;
	}


	/**
	 * @param timeout
	 *            the timeout to set
	 */
	public void setTimeout(final int timeout) {
		this.timeout = timeout;
	}

}
