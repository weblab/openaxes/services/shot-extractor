package eu.axes.services.shotextractor;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.springframework.core.io.FileSystemResource;

import eu.axes.utils.AxesAnnotator;


@SuppressWarnings({ "javadoc", "static-method" })
public class TestShotExtractor {


	private static final String EXECUTABLE = "src/test/resources/fakeExecutable";


	private static final String C_COLLECTION = "cCollection";


	private static final String TEST_SHORT_VIDEO = "vthe_daily_2011_08_04_cnn_short";


	private static final String TEST_SINGLE_SHOT_VIDEO = "vMalted_Shreddies";


	@Test
	public void testShortVideo() throws Exception {
		this.processVideo(TEST_SHORT_VIDEO);
	}


	@Test
	public void testSingleShotVideo() throws Exception {
		this.processVideo(TEST_SINGLE_SHOT_VIDEO);
	}


	private void processVideo(final String videoId) throws IOException, WebLabCheckedException, ContentNotAvailableException, InvalidParameterException, UnexpectedException,
			InsufficientResourcesException {

		final Properties props = new Properties();
		props.load(new FileSystemResource("src/test/resources/axesContentManager.properties").getInputStream());
		final File rootFolder = new File(props.get("rootFolder").toString());
		if (rootFolder.exists()) {
			FileUtils.cleanDirectory(rootFolder);
		}

		// Recreating an fake video file (to prevent from early failing in the service that checks its existence).
		final File testVideoFolder = new File(new File(rootFolder, C_COLLECTION), videoId);
		testVideoFolder.mkdirs();
		FileUtils.copyFile(new File("pom.xml"), new File(testVideoFolder, videoId + ".mp4"));

		final Document videoNameDocument = new WebLabMarshaller(true).unmarshal(new File("src/test/resources/samplesResources/" + videoId + ".xml"), Document.class);

		final ShotExtractorConfigBean config = new ShotExtractorConfigBean();
		config.setPathToFrameExtractor(new File(EXECUTABLE));
		config.setPathToOutFolder(new File("src/test/resources/samplesOutputs"));
		config.setPathToShotDetector(new File(EXECUTABLE));

		final ShotExtractor shotExtractor = new ShotExtractor(config);
		final ProcessArgs pa = new ProcessArgs();
		pa.setResource(videoNameDocument);
		Resource resource = shotExtractor.process(pa).getResource();
		new WebLabMarshaller(true).marshalResource(resource, new File("target/" + videoId + ".xml"));

		List<Image> images = ResourceUtil.getSelectedSubResources(resource, Image.class);
		Assert.assertTrue(images.size() > 0);
		for (final Image img : images) {
			final AxesAnnotator aa = new AxesAnnotator(img);
			Assert.assertTrue(aa.readCollectionId().hasValue());
			Assert.assertEquals(C_COLLECTION, aa.readCollectionId().firstTypedValue());
			Assert.assertTrue(aa.readVideoId().hasValue());
			Assert.assertEquals(videoId, aa.readVideoId().firstTypedValue());
			Assert.assertTrue(aa.readShotId().hasValue());
			Assert.assertTrue(aa.readShotId().firstTypedValue().startsWith("s"));
			Assert.assertTrue(aa.readFrameId().hasValue());
			Assert.assertTrue(aa.readFrameId().firstTypedValue().startsWith("keyframe"));
		}
	}

}
